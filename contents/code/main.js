var client;
var availableScreen;
var columnSize;
var rowSize;

var gridSize = 6;
var innerGap = 20;
var innerHalfGap = innerGap / 2;
var outerGap = 0;
var relativeOuterGap = outerGap - innerHalfGap;

var maximizeRespectsGap = false;
var alignNewWindow = true;

function setClientData() {
    client = workspace.activeClient;
}

function setScreenData() {
    availableScreen = workspace.clientArea(KWin.MaximizeArea, client);
    availableScreen.x += relativeOuterGap;
    availableScreen.y += relativeOuterGap;
    availableScreen.width -= 2 * relativeOuterGap;
    availableScreen.height -= 2 * relativeOuterGap;
    columnSize = availableScreen.width / gridSize;
    rowSize = availableScreen.height / gridSize;
}

function findNextGridColumn(x) {
    for (var i = 0; i < gridSize; i++) {
        var columnPosition = columnSize * i + availableScreen.x;
        columnPosition = Math.floor(columnPosition);
        if (columnPosition > x) {
            return columnPosition;
        }
    }
    return columnSize * (gridSize) + availableScreen.x;
}

function findPrevGridColumn(x) {
    for (var i = gridSize; i > 0; --i) {
        var columnPosition = columnSize * i + availableScreen.x;
        columnPosition = Math.ceil(columnPosition);
        if (columnPosition < x) {
            return columnPosition;
        }
    }
    return 0 + availableScreen.x;
}

function findNextGridRow(y) {
    for (var i = 0; i < gridSize; i++) {
        var rowPosition = rowSize * i + availableScreen.y;
        rowPosition = Math.floor(rowPosition);
        if (rowPosition > y) {
            return rowPosition;
        }
    }
    return rowSize * (gridSize) + availableScreen.y;
}

function findPrevGridRow(y) {
    for (var i = gridSize; i > 0; --i) {
        var rowPosition = rowSize * i + availableScreen.y;
        rowPosition = Math.ceil(rowPosition);
        if (rowPosition < y) {
            return rowPosition;
        }
    }
    return 0 + availableScreen.y;
}

function move(newX, newY) {
    if (!client.moveable) {
        return;
    }
    client.move = true;
    client.geometry = {
        x: newX,
        y: newY,
        width: client.geometry.width,
        height: client.geometry.height
    };
    client.move = false;
}

function resize(newWidth, newHeight) {
    if (!client.resizeable) {
        return;
    }
    client.resize = true;
    client.geometry = {
        x: client.geometry.x,
        y: client.geometry.y,
        width: newWidth,
        height: newHeight
    };
    client.resize = false;
}

function findCloserNumber(origin, n1, n2) {
    var diff1 = Math.abs(origin - n1);
    var diff2 = Math.abs(origin - n2);
    if (diff1 < diff2) {
        return n1;
    } else {
        return n2;
    }
}

function findClosestColumn(x) {
    var x_left = findPrevGridColumn(x);
    var x_right = findNextGridColumn(x);
    var closest = findCloserNumber(x, x_left, x_right);
    return closest;
}

function findClosestRow(y) {
    var y_top = findPrevGridRow(y);
    var y_bottom = findNextGridRow(y);
    var closest = findCloserNumber(y, y_top, y_bottom);
    return closest;
}

function alignRightWindowSide() {
    var newX = findClosestColumn(client.geometry.x + client.geometry.width);
    var newWidth = newX - client.geometry.x;
    if (newWidth >= columnSize - innerHalfGap) {
        resize(newWidth - innerHalfGap, client.geometry.height);
    }
}

function alignLeftWindowSide() {
    var newX = findClosestColumn(client.geometry.x);
    var xDiff = client.geometry.x - newX;
    var newWidth = client.geometry.width + xDiff;
    if (newWidth >= columnSize - innerHalfGap) {
        resize(newWidth - innerHalfGap, client.geometry.height);
    }
    if (newX >= availableScreen.x) {
        move(newX + innerHalfGap, client.geometry.y);
    }
}

function alignBottomWindowSide() {
    var newY = findClosestRow(client.geometry.y + client.geometry.height);
    var newHeight = newY - client.geometry.y;
    if (newHeight >= rowSize - innerHalfGap) {
        resize(client.geometry.width, newHeight - innerHalfGap);
    }
}

function alignTopWindowSide() {
    var newY = findClosestRow(client.geometry.y);
    var yDiff = client.geometry.y - newY;
    var newHeight = client.geometry.height + yDiff;
    if (newHeight >= rowSize - innerHalfGap) {
        resize(client.geometry.width, newHeight - innerHalfGap);
    }
    if (newY >= availableScreen.y) {
        move(client.geometry.x, newY + innerHalfGap);
    }
}

function isLeftAligned() {
    if ((client.geometry.x - innerHalfGap - availableScreen.x) % (availableScreen.width / gridSize) === 0) {
        return true;
    } else {
        return false
    }
}

function isRightAligned() {
    if ((client.geometry.x + client.geometry.width + innerHalfGap - availableScreen.x) % (availableScreen.width / gridSize) === 0) {
        return true;
    } else {
        return false
    }
}

function isTopAligned() {
    if ((client.geometry.y - innerHalfGap - availableScreen.y) % (availableScreen.height / gridSize) === 0) {
        return true;
    } else {
        return false
    }
}

function isBottomAligned() {
    if ((client.geometry.y + client.geometry.height + innerHalfGap - availableScreen.y) % (availableScreen.height / gridSize) === 0) {
        return true;
    } else {
        return false
    }
}

function align() {
    if (!isLeftAligned()) {
        alignLeftWindowSide();
    }
    if (!isRightAligned()) {
        alignRightWindowSide();
    }
    if (!isTopAligned()) {
        alignTopWindowSide();
    }
    if (!isBottomAligned()) {
        alignBottomWindowSide();
    }
}

function alignAll() {
    var currentDesktop = workspace.currentDesktop;
    var clients = workspace.clientList();
    for (var i = 0; i < clients.length; i++) {
        client = clients[i];
        if (client.desktop != currentDesktop) {
            continue;
        }
        if (!client.normalWindow) {
            continue;
        }
        setScreenData();
        align();
    }
}

function moveLeft() {
    setClientData();
    setScreenData();
    var newX = findPrevGridColumn(client.geometry.x - innerHalfGap);
    if (newX >= availableScreen.x) {
        move(newX + innerHalfGap, client.geometry.y);
    }
    align();
}

function moveRight() {
    setClientData();
    setScreenData();
    var newX = findNextGridColumn(client.geometry.x - innerHalfGap);
    if (newX < (availableScreen.x + availableScreen.width)) {
        move(newX + innerHalfGap, client.geometry.y);
    }
    align();
}

function moveUp() {
    setClientData();
    setScreenData();
    var newY = findPrevGridRow(client.geometry.y - innerHalfGap);
    if (newY >= availableScreen.y) {
        move(client.geometry.x, newY + innerHalfGap);
    }
    align();
}

function moveDown() {
    setClientData();
    setScreenData();
    var newY = findNextGridRow(client.geometry.y - innerHalfGap);
    if (newY < (availableScreen.y + availableScreen.height)) {
        move(client.geometry.x, newY + innerHalfGap);
    }
    align();
}

function increaseWidth() {
    setClientData();
    setScreenData();
    var nextColumn = findNextGridColumn(client.geometry.x + client.geometry.width + innerHalfGap);
    var newWidth = nextColumn - client.geometry.x;
    if (newWidth + client.geometry.x <= availableScreen.x + availableScreen.width) {
        resize(newWidth - innerHalfGap, client.geometry.height);
    }
    align();
}

function decreaseWidth() {
    setClientData();
    setScreenData();
    var prevColumn = findPrevGridColumn(client.geometry.x + client.geometry.width + innerHalfGap);
    var newWidth = prevColumn - client.geometry.x;
    if (newWidth >= columnSize - innerHalfGap) {
        resize(newWidth - innerHalfGap, client.geometry.height);
    }
    align();
}

function increaseHeight() {
    setClientData();
    setScreenData();
    var nextRow = findNextGridRow(client.geometry.y + client.geometry.height + innerHalfGap);
    var newHeight = nextRow - client.geometry.y;
    if (newHeight + client.geometry.y <= availableScreen.y + availableScreen.height) {
        resize(client.geometry.width, newHeight - innerHalfGap);
    }
    align();
}

function decreaseHeight() {
    setClientData();
    setScreenData();
    var prevRow = findPrevGridRow(client.geometry.y + client.geometry.height + innerHalfGap);
    var newHeight = prevRow - client.geometry.y;
    if (newHeight >= rowSize - innerHalfGap) {
        resize(client.geometry.width, newHeight - innerHalfGap);
    }
    align();
}

function halfTileLeft() {
    setClientData();
    setScreenData();
    move(0 + availableScreen.x + innerHalfGap, 0 + availableScreen.y + innerHalfGap);
    resize(availableScreen.width / 2 - innerGap, availableScreen.height - innerGap);
}

function halfTileRight() {
    setClientData();
    setScreenData();
    move(availableScreen.x + (availableScreen.width / 2) + innerHalfGap, 0 + availableScreen.y + innerHalfGap);
    resize(availableScreen.width / 2 - innerGap, availableScreen.height - innerGap);
}

function halfTileTop() {
    setClientData();
    setScreenData();
    move(0 + availableScreen.x + innerHalfGap, 0 + availableScreen.y + innerHalfGap);
    resize(availableScreen.width - innerGap, availableScreen.height / 2 - innerGap);
}

function halfTileBottom() {
    setClientData();
    setScreenData();
    move(0 + availableScreen.x + innerHalfGap, availableScreen.y + (availableScreen.height / 2) + innerHalfGap);
    resize(availableScreen.width - innerGap, availableScreen.height / 2 - innerGap);
}

function quarterTileTopLeft() {
    setClientData();
    setScreenData();
    move(0 + availableScreen.x + innerHalfGap, 0 + availableScreen.y + innerHalfGap);
    resize(availableScreen.width / 2 - innerGap, availableScreen.height / 2 - innerGap);
}

function quarterTileTopRight() {
    setClientData();
    setScreenData();
    move(availableScreen.x + (availableScreen.width / 2) + innerHalfGap, 0 + availableScreen.y + innerHalfGap);
    resize(availableScreen.width / 2 - innerGap, availableScreen.height / 2 - innerGap);
}

function quarterTileBottomRight() {
    setClientData();
    setScreenData();
    move(availableScreen.x + (availableScreen.width / 2) + innerHalfGap, availableScreen.y + (availableScreen.height / 2) + innerHalfGap);
    resize(availableScreen.width / 2 - innerGap, availableScreen.height / 2 - innerGap);
}

function quarterTileBottomLeft() {
    setClientData();
    setScreenData();
    move(0 + availableScreen.x + innerHalfGap, availableScreen.y + (availableScreen.height / 2) + innerHalfGap);
    resize(availableScreen.width / 2 - innerGap, availableScreen.height / 2 - innerGap);
}

function maximize() {
    setClientData();
    setScreenData();
    if (maximizeRespectsGap) {
        move(availableScreen.x + innerHalfGap, availableScreen.y + innerHalfGap);
        resize(availableScreen.width - 2 * innerHalfGap, availableScreen.height - 2 * innerHalfGap);
    } else {
        move(availableScreen.x - relativeOuterGap, availableScreen.y - relativeOuterGap);
        resize(availableScreen.width + 2 * relativeOuterGap, availableScreen.height + 2 * relativeOuterGap);
    }
}

workspace.clientAdded.connect(function (newClient) {
    if (alignNewWindow) {
        if (!newClient.normalWindow) {
            return;
        }
        client = newClient;
        setScreenData();
        align();
    }
});

registerShortcut("GridMoveWindowLeft", "Grid: move window left", "Meta+Ctrl+H", function () {
    moveLeft();
});

registerShortcut("GridMoveWindowRight", "Grid: move window right", "Meta+Ctrl+L", function () {
    moveRight();
});

registerShortcut("GridMoveWindowUp", "Grid: move window up", "Meta+Ctrl+K", function () {
    moveUp();
});

registerShortcut("GridMoveWindowDown", "Grid: move window down", "Meta+Ctrl+J", function () {
    moveDown();
});

registerShortcut("GridIncreaseWidth", "Grid: increase window width", "Meta+Shift+L", function () {
    increaseWidth();
});

registerShortcut("GridDecreaseWidth", "Grid: decrease window width", "Meta+Shift+H", function () {
    decreaseWidth();
});

registerShortcut("GridIncreaseHeight", "Grid: increase window height", "Meta+Shift+J", function () {
    increaseHeight();
});

registerShortcut("GridDecreaseHeight", "Grid: decrease window height", "Meta+Shift+K", function () {
    decreaseHeight();
});

registerShortcut("GridHalfTileLeft", "Grid: half tile window left", "Meta+Alt+H", function () {
    halfTileLeft();
});

registerShortcut("GridHalfTileRight", "Grid: half tile window right", "Meta+Alt+L", function () {
    halfTileRight();
});

registerShortcut("GridHalfTileTop", "Grid: half tile window top", "Meta+Alt+K", function () {
    halfTileTop();
});

registerShortcut("GridHalfTileBottom", "Grid: half tile window bottom", "Meta+Alt+J", function () {
    halfTileBottom();
});

registerShortcut("GridQuarterTileTopLeft", "Grid: quarter tile window top left", "Meta+Alt+Shift+H", function () {
    quarterTileTopLeft();
});

registerShortcut("GridQuarterTileTopRight", "Grid: quarter tile window top right", "Meta+Alt+Shift+K", function () {
    quarterTileTopRight();
});

registerShortcut("GridQuarterTileBottomRight", "Grid: quarter tile window bottom right", "Meta+Alt+Shift+L", function () {
    quarterTileBottomRight();
});

registerShortcut("GridQuarterTileBottomLeft", "Grid: quarter tile window bottom left", "Meta+Alt+Shift+J", function () {
    quarterTileBottomLeft();
});

registerShortcut("GridAlign", "Grid: align window to grid", "Meta+.", function () {
    setClientData();
    setScreenData();
    align();
});

registerShortcut("GridAlignAll", "Grid: align all windows of current desktop to grid", "Meta+,", function () {
    alignAll();
});

registerShortcut("GridMaximize", "Grid: maximize window", "Meta+M", function () {
    maximize();
});
